var specialCodeController = new function(){

    var fieldLabel = "ppeCode : " ;
    var propertyName = "custom4";
    var defaultSpecialCode = "DEF201920" ;
    var blankText = "" ;
    var validation = "";

    var btnDefault = "btnDefault";
    var btnChange = "btnChange";
    var btnCancel = "btnCancel";
    var btnAccept = "btnAccept";
    var lblSpecialCode = "lblSpecialCode";
    var txtSpecialCode = "txtSpecialCode";
    var currentMode = "";

    function defaultMode()
    {
        currentMode = "defaultMode";

        $('#'+txtSpecialCode).hide();
        $('#'+lblSpecialCode).show();

        $('#'+btnCancel).hide();
        $('#'+btnAccept).hide();
        $('#'+btnChange).show();
        $('#'+btnDefault).hide();
        $('#specialCodePanelDefaultCheckboxLabel').hide();
   }

    function changingMode() {
        currentMode = "changingMode";

        $('#' + txtSpecialCode).show();
        $('#' + lblSpecialCode).hide();

        var tempSpecialCode = $('#' + lblSpecialCode).text();
        document.getElementById(txtSpecialCode).value = tempSpecialCode;

        $('#' + btnCancel).hide();
        $('#' + btnAccept).show();
        $('#' + btnChange).hide();

        if (defaultSpecialCode !== "")
        {
            $('#' + btnDefault).show();
            $('#specialCodePanelDefaultCheckboxLabel').show();
        }
    }

    function setDefaultSpecialCode() {
        document.getElementById(txtSpecialCode).value = defaultSpecialCode;
        //$('#'+txtSpecialCode).attr("value", defaultSpecialCode);
        $('#'+txtSpecialCode).attr("disabled",true);
    }



   this.init = function(config)
   {

       fieldLabel = config.fieldLabel;
       propertyName = config.propertyName;
       defaultSpecialCode = config.defaultSpecialCode;
       blankText = config.blankText;
       validation = config.validation;


       $('#specialCodePanel').html(
           "<span>"+fieldLabel+" : </span>" +
           "<span id='"+lblSpecialCode+"'> </span>" +
           "<input type='text' id='"+txtSpecialCode+"'/>" +
           "<input class='iflow_checkbox' type='checkbox' id='"+btnDefault+"'> <label id='specialCodePanelDefaultCheckboxLabel'> " + jQuery.i18n.prop('info.specialPanel.defaultCheckbox')+"</label>" +
           "<button class='iflow_button icon-edit'id='"+btnChange+"'></button>" +
           "<button class='iflow_button icon-edit' id='"+btnCancel+"'>Cancel</button>" +
           "<button class='iflow_button icon-check' id='"+btnAccept+"'></button>"

       );


       if(defaultSpecialCode === "")
       {
           $('#'+btnDefault).hide();
           $('#specialCodePanelDefaultCheckboxLabel').hide();
       }

       $('#specialCodePanel').click(function (ele) {
           switch(ele.target.id)
           {
               case btnDefault :
                   if(ele.target.checked)
                   {
                       setDefaultSpecialCode();
                   }
                   else
                   {
                       document.getElementById(txtSpecialCode).value = "";
                       $('#'+txtSpecialCode).attr("disabled",false);
                   }
                   break;
               case btnAccept :
                   setSpecialCodeToCurrentVisit();
                   defaultMode();
                   break;
               case btnChange :
                   changingMode();
                   break;
               case btnCancel :
                   defaultMode();
                   break;
           }
       })

       //de();
   };

   this.getSpecialCodeFromCurrentVisit = function () {
       getSpecialCodeFromCurrentVisit();
   };


    function setSpecialCodeToCurrentVisit() {

        var specialCode1 = "";
        var params = {};

        if(currentMode == "changingMode" ) {
            var _specialCode = document.getElementById(txtSpecialCode).value;
            $('#'+lblSpecialCode).text(_specialCode);
        }

        specialCode = $('#'+lblSpecialCode).text();

        params.json = '{"'+propertyName+'":"'+ specialCode + '"}';

        var response = spService.putParams('branches/'+sessvars.state.branchId+'/visits/'+sessvars.state.visit.id+'/parameters/',params);


        if(response.parameterMap && response.parameterMap[propertyName])
        {
            $('#'+lblSpecialCode).text(response.parameterMap[propertyName]);
        }
        else {
            $('#'+lblSpecialCode).text("");
        }
    }


    function getSpecialCodeFromCurrentVisit() {

        if(sessvars.state.visit === null)
        {
            return;
        }

       var response = spService.get('branches/'+sessvars.state.branchId+'/visits/'+sessvars.state.visit.id);

       if(response.parameterMap && response.parameterMap[propertyName])
       {
           $('#'+lblSpecialCode).text(response.parameterMap[propertyName]);
           defaultMode();
       }
       else {
           document.getElementById(txtSpecialCode).value = "";
           $('#'+lblSpecialCode).text("");
           $('#'+txtSpecialCode).attr("disabled",false);
           document.getElementById("btnDefault").checked = false;
           changingMode();
       }
   }

};

