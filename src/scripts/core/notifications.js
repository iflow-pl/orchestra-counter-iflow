var iflowNotifications = new function(){
    
    var channel = null;
    
    var titleDiv = null;
    var contentDiv = null;
    var senderLabelDiv = null;
    
    var onNotification = function(data){
        var title = data ? (data.title ? data.title : '') : '';
        var content = data ? (data.content ? data.content : '') : '';
        var senderLabel = data ? (data.senderLabel ? data.senderLabel : '') : '';
        if(titleDiv !== null)
            titleDiv.text(title);
        if(contentDiv !== null)
            contentDiv.text(content);
        if(senderLabelDiv !== null)
            senderLabelDiv.text(senderLabel);

        modalNavigationController.push($Qmatic.components.modal.iflowNotification);
    };
    
    var branchChanged = function(){
        if(channel !== null){
            qevents.unsubscribe(channel);
            channel = null;
        }
        channel = 'IFLOW/APPLICATION/' + sessvars.branchId + '/notifications/SERVICEPOINT#' + sessvars.servicePointUnitId;
        channel = channel.replace(":","/");
        qevents.subscribe(channel, function(data){
            if(typeof(data) === 'string')
                data = JSON.parse(data);
            onNotification(data);
        });
    };
    

        this.init = function(){
            titleDiv = $('#iflowNotifications_title');
            contentDiv = $('#iflowNotifications_content');
            senderLabelDiv = $('#iflowNotifications_sender');
            branchChanged();
        };
        
        this.branchChanged =  branchChanged
    
};